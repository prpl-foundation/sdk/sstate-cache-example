# LCM sstate cache server builder

[[_TOC_]]

## Introduction

An sstate cache server is essential in Yocto builds to mitigate lengthy build times, especially in large projects or collaborative environments. It stores intermediate build artifacts centrally, allowing developers to reuse precompiled packages instead of compiling from source repeatedly. This reduces build times, conserves resources, facilitates collaboration, and accelerates CI/CD pipelines. In essence, an sstate cache server optimizes Yocto builds by minimizing redundant compilation steps and improving overall development efficiency.

Presented here is a basic sample of an sstate cache server, which can be constructed as a Docker container and deployed on the same machine where the SDK is built or utilized. The server's default configuration aligns with the default setup of the LCM SDK builder. However, it's important to note that this example serves purely as a demonstration and should not be used as the foundation for an actual sstate cache server.

## Building the sstate cache

To build the sstate cache server, follow these steps:

```
docker build -t lcm-sstate-cache .
```

## Running the sstate cache

To deploy the sstate cache server, utilize the provided Docker Compose file:

```
cd compose
docker-compose up -d
```