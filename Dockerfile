# Use a base image with necessary packages installed
FROM ubuntu:22.04

# Install required packages
RUN apt-get update && \
    apt-get install -y \
    python3 \
    python3-pip \
    openssh-server \
    rsync \
    vim

# Install required Python packages
RUN pip3 install flask

# Expose the port
EXPOSE 8000
EXPOSE 22

# Generate SSH keys
RUN ssh-keygen -A
    
# Set up sstate cache directory
RUN mkdir -p /srv/sstate-cache

RUN echo 'root:mycacheserverpassword' | chpasswd
RUN mkdir /var/run/sshd

# Copy sstate server script into the container
COPY sstate_server.py /srv/sstate_server.py

COPY startup.sh /usr/bin/startup.sh

## Enable ssh password authentication with root user
RUN echo "PasswordAuthentication yes" >> /etc/ssh/sshd_config
RUN echo "PermitRootLogin yes" >> /etc/ssh/sshd_config

# Start sstate server
CMD ["/usr/bin/startup.sh"]
