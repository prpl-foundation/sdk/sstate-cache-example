from flask import Flask, send_file
import os

app = Flask(__name__)

@app.route('/<path:filename>')
def download_file(filename):
    return send_file(os.path.join('/', filename))

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000)
